/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  vitalik
 * Created: Nov 18, 2020
 */

create table aircrafts (
  aircraft_code CHAR(3) not null primary key,
  model TEXT not null,
  range INT not null
);

create table airports (
  airport_code CHAR(3) not null primary key,
  airport_name TEXT not null,
  city TEXT not null,
  coordinates TEXT not null,
  timezone TEXT not null
);

create table bookings (
  book_ref CHAR(6) not null primary key,
  book_date TEXT not null,
  total_amount DECIMAL(10, 2) not null
);

create table flights (
  flight_id INT not null primary key,
  flight_no CHAR(6) not null,
  scheduled_departure TEXT not null,
  scheduled_arrival TEXT not null,
  departure_airport CHAR(3) not null,
  arrival_airport CHAR(3) not null,
  status VARCHAR(20) not null,
  aircraft_code CHAR(3) not null,
  actual_departure TEXT,
  actual_arrival TEXT,
  foreign key (aircraft_code) references aircrafts(aircraft_code),
  foreign key (arrival_airport) references airports(airport_code),
  foreign key (departure_airport) references airports(airport_code)
);

create table seats (
  aircraft_code CHAR(3) not null,
  seat_no VARCHAR(4) not null,
  fare_conditions VARCHAR(10) not null,
  primary key (aircraft_code, seat_no),
  foreign key (aircraft_code) references aircrafts(aircraft_code)
);

create table tickets (
  ticket_no CHAR(13) not null primary key,
  book_ref CHAR(6) not null,
  passenger_id VARCHAR(20) not null,
  passenger_name TEXT not null,
  contact_data TEXT,
  foreign key (book_ref) references bookings(book_ref)
);

create table ticket_flights (
  ticket_no CHAR(13) not null,
  flight_id INT not null,
  fare_conditions VARCHAR(10) not null,
  amount DECIMAL(10, 2) not null,
  primary key (ticket_no, flight_id),
  foreign key (ticket_no) references tickets(ticket_no),
  foreign key (flight_id) references flights(flight_id)
);

create table boarding_passes (
  ticket_no CHAR(13) not null,
  flight_id int not null,
  boarding_no INT not null,
  seat_no VARCHAR(4) not null, 
  primary key (ticket_no, flight_id), 
  foreign key (ticket_no) references ticket_flights(ticket_no),
  foreign key (flight_id) references ticket_flights(flight_id)
);