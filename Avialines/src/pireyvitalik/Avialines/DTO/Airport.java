/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import lombok.Data;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class Airport {
    private final String airportCode;
    private final HashMap<String, String> airportName;
    private final HashMap<String, String> city;
    private final String coordinates;
    private final String timezone;
    
    public Airport(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\w\\w\\w),\"(.*)\",\"(.*)\",\"(.*)\",(.*)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        airportCode = matcher.group(1);
        airportName = gson.fromJson(matcher.group(2).replace("\"\"", "\""), type);
        city = gson.fromJson(matcher.group(3).replace("\"\"", "\""), type);
        coordinates = matcher.group(4);
        timezone = matcher.group(5);
    }
}
