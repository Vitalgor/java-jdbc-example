/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;

import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class Seat {
    private final String aircraftCode;
    private final String seatNo;
    private final String fareConditions;
    
    public Seat(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\w+),(\\w+),(\\w+)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        aircraftCode = matcher.group(1);
        seatNo = matcher.group(2);
        fareConditions = matcher.group(3);
    }
}
