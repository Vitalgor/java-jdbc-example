/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;

import pireyvitalik.Avialines.InvalidParseException;

/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class BoardingPass {
    private final String ticketNo;
    private final int flightId;
    private final int boardingNo;
    private final String seatNo;
    
    public BoardingPass(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\w+),(\\d+),(\\d+),(\\w+)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        ticketNo = matcher.group(1);
        flightId = Integer.parseInt(matcher.group(2));
        boardingNo = Integer.parseInt(matcher.group(3));
        seatNo = matcher.group(4);
    }
}
