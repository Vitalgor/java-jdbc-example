/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.HashMap;
import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;

import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@AllArgsConstructor
@Data
public class Aircraft {
    private final String aircraftCode;
    private final HashMap<String, String>  model;
    private final int range;
    
    public Aircraft(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\w\\w\\w),\"(.*)\",(\\d+)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        aircraftCode = matcher.group(1);
        model = gson.fromJson(matcher.group(2).replace("\"\"", "\""), type);
        range = Integer.parseInt(matcher.group(3));
    }
}
