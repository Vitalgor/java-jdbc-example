/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;

import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class Ticket {
    private final String ticketNo;
    private final String bookRef;
    private final String passengerId;
    private final String passengerName;
    private final HashMap<String, String> contactData;
    
    public Ticket(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(.+),(.+),(.+),(.+),\"(.*)\"");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        ticketNo = matcher.group(1);
        bookRef = matcher.group(2);
        passengerId = matcher.group(3);
        passengerName = matcher.group(4);
        if (matcher.group(5).length() > 0)
            contactData = gson.fromJson(matcher.group(5).replace("\"\"", "\""), type);
        else
            contactData = null;
    }
}
