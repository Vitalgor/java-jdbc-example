/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;

import lombok.Data;
import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;

import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class Flight {
    private final int flightId;
    private final String flightNo;
    private final Timestamp scheduledDeparture;
    private final Timestamp scheduledArrival;
    private final String departureAirport;
    private final String arrivalAirport;
    private final String status;
    private final String aircraftCode;
    private final Timestamp actualDeparture;
    private final Timestamp actualArrival;
    
    public Flight(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\d+),(.+),(.+)\\+\\d\\d,(.+)\\+\\d\\d,(\\w\\w\\w),(\\w\\w\\w),(.+),(\\w\\w\\w),(.*),(.*)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        flightId = Integer.parseInt(matcher.group(1));
        flightNo = matcher.group(2);
        scheduledDeparture = Timestamp.valueOf(matcher.group(3));
        scheduledArrival = Timestamp.valueOf(matcher.group(4));
        departureAirport = matcher.group(5);
        arrivalAirport = matcher.group(6);
        status = matcher.group(7);
        aircraftCode = matcher.group(8);
        if (!matcher.group(9).equals(""))
            actualDeparture = Timestamp.valueOf(matcher.group(9).split("\\+")[0]);
        else
            actualDeparture = null;
        if (!matcher.group(10).equals(""))
            actualArrival = Timestamp.valueOf(matcher.group(10).split("\\+")[0]);
        else
            actualArrival = null;
            
    }
}
