/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;

import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class TicketFlight {
    private final String ticketNo;
    private final int flightId;
    private final String fareConditions;
    private final double amount;
    
    public TicketFlight(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\d+),(\\d+),(\\w+),(\\d+\\.\\d\\d)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        ticketNo = matcher.group(1);
        flightId = Integer.parseInt(matcher.group(2));
        fareConditions = matcher.group(3);
        amount = Double.parseDouble(matcher.group(4));
    }
}
