/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DTO;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import lombok.Data;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;

import pireyvitalik.Avialines.InvalidParseException;
/**
 *
 * @author vitalik
 */
@Data
@AllArgsConstructor
public class Booking {
    private final String bookRef;
    private final Timestamp bookDate;
    private final double totalAmount;
    
    public Booking(String csv) throws InvalidParseException{
        Pattern pattern = Pattern.compile("(\\w+),(.+)\\+\\d\\d,(\\d+\\.\\d\\d)");
        Matcher matcher = pattern.matcher(csv);
        if (!matcher.matches()){
            throw new InvalidParseException();
        }
        bookRef = matcher.group(1);
        bookDate = Timestamp.valueOf(matcher.group(2));
        totalAmount = Double.parseDouble(matcher.group(3));
    }
}
