/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import pireyvitalik.Avialines.DTO.BoardingPass;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class BoardingPassDAO {
    private final DataSource connectionPool;
    
    public BoardingPass createBoardingPass(ResultSet resultSet) throws SQLException {
        return new BoardingPass(resultSet.getString("ticket_no"),
                resultSet.getInt("flight_id"), 
                resultSet.getInt("boarding_no"),
                resultSet.getString("seat_no"));
    }
    
    public void saveBoardingPassess(Collection<BoardingPass> data) throws SQLException {
        String sql = "insert into boarding_passes(ticket_no, flight_id, boarding_no, seat_no) values (?, ?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (BoardingPass aircraft : data){
                stmt.setString(1, aircraft.getTicketNo());
                stmt.setInt(2, aircraft.getFlightId());
                stmt.setInt(3, aircraft.getBoardingNo());
                stmt.setString(4, aircraft.getSeatNo());
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveBoardingPassess(Collection<BoardingPass> data, Connection conn) throws SQLException {
        String sql = "insert into boarding_passes(ticket_no, flight_id, boarding_no, seat_no) values (?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (BoardingPass aircraft : data){
                stmt.setString(1, aircraft.getTicketNo());
                stmt.setInt(2, aircraft.getFlightId());
                stmt.setInt(3, aircraft.getBoardingNo());
                stmt.setString(4, aircraft.getSeatNo());
                stmt.execute();
            }
        }
    }
    
    public Set<BoardingPass> getBoardingPass() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<BoardingPass> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from boarding_passes");
            while (resultSet.next()) {
                result.add(createBoardingPass(resultSet));
            }
            return result;
        }
    }
}
