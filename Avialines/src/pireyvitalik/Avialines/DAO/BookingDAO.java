/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import java.sql.Timestamp;
import pireyvitalik.Avialines.DTO.Booking;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class BookingDAO {
    private final DataSource connectionPool;
    
    public Booking createBooking(ResultSet resultSet) throws SQLException {
        return new Booking(resultSet.getString("book_ref"),
                Timestamp.valueOf(resultSet.getString("book_date")), 
                resultSet.getDouble("total_amount"));
    }
    
    public void saveBookings(Collection<Booking> data) throws SQLException {
        String sql = "insert into bookings(book_ref, book_date, total_amount) values (?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (Booking booking : data){
                stmt.setString(1, booking.getBookRef());
                stmt.setString(2, booking.getBookDate().toString());
                stmt.setDouble(3, booking.getTotalAmount());
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveBookings(Collection<Booking> data, Connection conn) throws SQLException {
        String sql = "insert into bookings(book_ref, book_date, total_amount) values (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (Booking booking : data){
                stmt.setString(1, booking.getBookRef());
                stmt.setString(2, booking.getBookDate().toString());
                stmt.setDouble(3, booking.getTotalAmount());
                stmt.execute();
            }
        }
    }
    
    public Set<Booking> getBooking() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<Booking> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from bookings");
            while (resultSet.next()) {
                result.add(createBooking(resultSet));
            }
            return result;
        }
    }
}
