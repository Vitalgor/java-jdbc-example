/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import pireyvitalik.Avialines.DTO.Flight;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class FlightDAO {
    private final DataSource connectionPool;
    
    public Flight createFlight(ResultSet resultSet) throws SQLException {
        return new Flight(resultSet.getInt("flight_id"),
                resultSet.getString("flight_no"),
                Timestamp.valueOf(resultSet.getString("scheduled_departure")), 
                Timestamp.valueOf(resultSet.getString("scheduled_arrival")), 
                resultSet.getString("departure_airport"),
                resultSet.getString("arrival_airport"),
                resultSet.getString("status"),
                resultSet.getString("aircraft_code"),
                resultSet.getString("actual_departure") != null ? Timestamp.valueOf(resultSet.getString("actual_departure")) : null, 
                resultSet.getString("actual_arrival") != null ? Timestamp.valueOf(resultSet.getString("actual_arrival")): null);
    }
    
    public void saveFlights(Collection<Flight> data) throws SQLException {
        String sql = "insert into flights(flight_id, flight_no, scheduled_departure, scheduled_arrival,"
                + "departure_airport, arrival_airport, status, aircraft_code, actual_departure, actual_arrival"
                + ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (Flight flight : data){
                stmt.setInt(1, flight.getFlightId());
                stmt.setString(2, flight.getFlightNo());
                stmt.setString(3, flight.getScheduledDeparture().toString());
                stmt.setString(4, flight.getScheduledArrival().toString());
                stmt.setString(5, flight.getDepartureAirport());
                stmt.setString(6, flight.getArrivalAirport());
                stmt.setString(7, flight.getStatus());
                stmt.setString(8, flight.getAircraftCode());
                if (flight.getActualDeparture() != null)
                    stmt.setString(9, flight.getActualDeparture().toString());
                else 
                    stmt.setString(9, null);
                
                if (flight.getActualArrival() != null)
                    stmt.setString(10, flight.getActualArrival().toString());
                else 
                    stmt.setString(10, null);     
                
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveFlights(Collection<Flight> data, Connection conn) throws SQLException {
        String sql = "insert into flights(flight_id, flight_no, scheduled_departure, scheduled_arrival,"
                + "departure_airport, arrival_airport, status, aircraft_code, actual_departure, actual_arrival"
                + ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (Flight flight : data){
                stmt.setInt(1, flight.getFlightId());
                stmt.setString(2, flight.getFlightNo());
                stmt.setString(3, flight.getScheduledDeparture().toString());
                stmt.setString(4, flight.getScheduledArrival().toString());
                stmt.setString(5, flight.getDepartureAirport());
                stmt.setString(6, flight.getArrivalAirport());
                stmt.setString(7, flight.getStatus());
                stmt.setString(8, flight.getAircraftCode());
                if (flight.getActualDeparture() != null)
                    stmt.setString(9, flight.getActualDeparture().toString());
                else 
                    stmt.setString(9, null);
                
                if (flight.getActualArrival() != null)
                    stmt.setString(10, flight.getActualArrival().toString());
                else 
                    stmt.setString(10, null);     
                
                stmt.execute();
            }
        }
    }
    
    public Set<Flight> getFlight() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<Flight> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from flights");
            while (resultSet.next()) {
                result.add(createFlight(resultSet));
            }
            return result;
        }
    }
}
