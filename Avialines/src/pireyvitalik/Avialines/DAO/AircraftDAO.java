/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import pireyvitalik.Avialines.DTO.Aircraft;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.*;
import javax.sql.DataSource;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class AircraftDAO {
    private final DataSource connectionPool;
    
    public Aircraft createAircraft(ResultSet resultSet) throws SQLException {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        return new Aircraft(resultSet.getString("aircraft_code"),
                (HashMap<String, String> )gson.fromJson(resultSet.getString("model"), type), 
                resultSet.getInt("range"));
    }
    
    public void saveAircrafts(Collection<Aircraft> data) throws SQLException {
        Gson gson = new Gson();
        String sql = "insert into aircrafts(aircraft_code, model, range) values (?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (Aircraft aircraft : data){
                stmt.setString(1, aircraft.getAircraftCode());
                stmt.setString(2, gson.toJson(aircraft.getModel()));
                stmt.setInt(3, aircraft.getRange());
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveAircrafts(Collection<Aircraft> data, Connection conn) throws SQLException {
        Gson gson = new Gson();
        String sql = "insert into aircrafts(aircraft_code, model, range) values (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (Aircraft aircraft : data){
                stmt.setString(1, aircraft.getAircraftCode());
                stmt.setString(2, gson.toJson(aircraft.getModel()));
                stmt.setInt(3, aircraft.getRange());
                stmt.execute();
            }
        }
    }
    
    public Set<Aircraft> getAircraft() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<Aircraft> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from aircrafts");
            while (resultSet.next()) {
                result.add(createAircraft(resultSet));
            }
            return result;
        }
    }
}
