/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import pireyvitalik.Avialines.DTO.TicketFlight;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class TicketFlightDAO {
    private final DataSource connectionPool;
    
    public TicketFlight createTicketFlight(ResultSet resultSet) throws SQLException {
        return new TicketFlight(resultSet.getString("ticket_no"),
                resultSet.getInt("flight_id"),
                resultSet.getString("fare_conditions"),
                resultSet.getDouble("amount"));
    }
    
    public void saveTicketFlights(Collection<TicketFlight> data) throws SQLException {
        String sql = "insert into ticket_flights(ticket_no, flight_id, fare_conditions, amount) values (?, ?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (TicketFlight seat : data){
                stmt.setString(1, seat.getTicketNo());
                stmt.setInt(2, seat.getFlightId());
                stmt.setString(3, seat.getFareConditions());    
                stmt.setDouble(4, seat.getAmount());
                
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveTicketFlights(Collection<TicketFlight> data, Connection conn) throws SQLException {
        String sql = "insert into ticket_flights(ticket_no, flight_id, fare_conditions, amount) values (?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (TicketFlight seat : data){
                stmt.setString(1, seat.getTicketNo());
                stmt.setInt(2, seat.getFlightId());
                stmt.setString(3, seat.getFareConditions());    
                stmt.setDouble(4, seat.getAmount());
                
                stmt.execute();
            }
        }
    }
    
    public Set<TicketFlight> getTicketFlight() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<TicketFlight> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from ticket_flights");
            while (resultSet.next()) {
                result.add(createTicketFlight(resultSet));
            }
            return result;
        }
    }
}
