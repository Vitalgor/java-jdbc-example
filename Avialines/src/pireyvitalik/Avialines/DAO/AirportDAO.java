/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import pireyvitalik.Avialines.DTO.Airport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import lombok.AllArgsConstructor;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class AirportDAO {
    private final DataSource connectionPool;
    
    public Airport createAirport(ResultSet resultSet) throws SQLException {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        return new Airport(resultSet.getString("airport_code"),
                (HashMap<String, String> )gson.fromJson(resultSet.getString("airport_name"), type), 
                (HashMap<String, String> )gson.fromJson(resultSet.getString("city"), type),
                resultSet.getString("coordinates"),
                resultSet.getString("timezone")
                );
    }
    
    public void saveAirports(Collection<Airport> data) throws SQLException {
        Gson gson = new Gson();
        String sql = "insert into airports(airport_code, airport_name, city, coordinates, timezone) values (?, ?, ?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (Airport airport : data){
                stmt.setString(1, airport.getAirportCode());
                stmt.setString(2, gson.toJson(airport.getAirportName()));
                stmt.setString(3, gson.toJson(airport.getCity()));
                stmt.setString(4, airport.getCoordinates());
                stmt.setString(5, airport.getTimezone());
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveAirports(Collection<Airport> data, Connection conn) throws SQLException {
        Gson gson = new Gson();
        String sql = "insert into airports(airport_code, airport_name, city, coordinates, timezone) values (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (Airport airport : data){
                stmt.setString(1, airport.getAirportCode());
                stmt.setString(2, gson.toJson(airport.getAirportName()));
                stmt.setString(3, gson.toJson(airport.getCity()));
                stmt.setString(4, airport.getCoordinates());
                stmt.setString(5, airport.getTimezone());
                stmt.execute();
            }
        }
    }
    
    public Set<Airport> getAirport() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<Airport> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from airports");
            while (resultSet.next()) {
                result.add(createAirport(resultSet));
            }
            return result;
        }
    }
}
