/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import pireyvitalik.Avialines.DTO.Ticket;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class TicketDAO {
    private final DataSource connectionPool;
    
    public Ticket createTicket(ResultSet resultSet) throws SQLException {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        return new Ticket(resultSet.getString("ticket_no"),
                resultSet.getString("book_ref"),
                resultSet.getString("passenger_id"),
                resultSet.getString("passenger_name"),
                resultSet.getString("contact_data") != null ? (HashMap<String, String> )gson.fromJson(resultSet.getString("contact_data"), type) : null);
    }
    
    public void saveTickets(Collection<Ticket> data) throws SQLException {
        Gson gson = new Gson();
        String sql = "insert into tickets(ticket_no, book_ref, passenger_id, passenger_name, contact_data) values (?, ?, ?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (Ticket seat : data){
                stmt.setString(1, seat.getTicketNo());
                stmt.setString(2, seat.getBookRef());
                stmt.setString(3, seat.getPassengerId());    
                stmt.setString(4, seat.getPassengerName());
                if (seat.getContactData() == null)
                    stmt.setString(5, null);
                else
                    stmt.setString(5, gson.toJson(seat.getContactData())); 
                
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveTickets(Collection<Ticket> data, Connection conn) throws SQLException {
        Gson gson = new Gson();
        String sql = "insert into tickets(ticket_no, book_ref, passenger_id, passenger_name, contact_data) values (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (Ticket seat : data){
                stmt.setString(1, seat.getTicketNo());
                stmt.setString(2, seat.getBookRef());
                stmt.setString(3, seat.getPassengerId());    
                stmt.setString(4, seat.getPassengerName());
                if (seat.getContactData() == null)
                    stmt.setString(5, null);
                else
                    stmt.setString(5, gson.toJson(seat.getContactData())); 
                
                stmt.execute();
            }
        }
    }
    
    public Set<Ticket> getTicket() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<Ticket> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from tickets");
            while (resultSet.next()) {
                result.add(createTicket(resultSet));
            }
            return result;
        }
    }
}
