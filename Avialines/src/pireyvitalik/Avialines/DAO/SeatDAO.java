/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import pireyvitalik.Avialines.DTO.Seat;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class SeatDAO {
    private final DataSource connectionPool;
    
    public Seat createSeat(ResultSet resultSet) throws SQLException {
        return new Seat(resultSet.getString("aircraft_code"),
                resultSet.getString("seat_no"),
                resultSet.getString("fare_conditions"));
    }
    
    public void saveSeats(Collection<Seat> data) throws SQLException {
        String sql = "insert into seats(aircraft_code, seat_no, fare_conditions) values (?, ?, ?)";
        try (Connection conn = connectionPool.getConnection();  
             PreparedStatement stmt = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for (Seat seat : data){
                stmt.setString(1, seat.getAircraftCode());
                stmt.setString(2, seat.getSeatNo());
                stmt.setString(3, seat.getFareConditions());    
                
                stmt.execute();
            }
            conn.commit();
        }
    }
    
    public void saveSeats(Collection<Seat> data, Connection conn) throws SQLException {
        String sql = "insert into seats(aircraft_code, seat_no, fare_conditions) values (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            for (Seat seat : data){
                stmt.setString(1, seat.getAircraftCode());
                stmt.setString(2, seat.getSeatNo());
                stmt.setString(3, seat.getFareConditions());    
                
                stmt.execute();
            }
        }
    }
    
    public Set<Seat> getSeat() throws SQLException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            Set<Seat> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from seats");
            while (resultSet.next()) {
                result.add(createSeat(resultSet));
            }
            return result;
        }
    }
}
