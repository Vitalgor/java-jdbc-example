/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.Set;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.HashMap;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import pireyvitalik.Avialines.DAO.*;
import pireyvitalik.Avialines.DTO.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartUtils;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class DBClient {
    private final DataSource connectionPool;
    
    public void getCitiesWithMultipleAirports() throws SQLException, IOException{
        Set<Airport> airports = new AirportDAO(connectionPool).getAirport();
        HashMap<String, String> airportsByCities = new HashMap<>();
        HashMap<String, Integer> cityAirportsCount = new HashMap<>();
        for (Airport airport: airports) {
            if (airportsByCities.get(airport.getCity().get("ru")) != null)
                airportsByCities.put(airport.getCity().get("ru"), 
                        airportsByCities.get(airport.getCity().get("ru")).concat(", "+airport.getAirportCode()));
            else
                airportsByCities.put(airport.getCity().get("ru"), airport.getAirportCode());
            if (cityAirportsCount.get(airport.getCity().get("ru")) != null)
                cityAirportsCount.put(airport.getCity().get("ru"), cityAirportsCount.get(airport.getCity().get("ru")) + 1);
            else
                cityAirportsCount.put(airport.getCity().get("ru"), 1);
        }
        
        String filename = "cities_with_multiple_airports.xlsx";
        FileOutputStream fileOutputStream = new FileOutputStream(filename);
        XSSFWorkbook report = new XSSFWorkbook();
        XSSFSheet sheet = report.createSheet("cities");

        XSSFRow row = null;
        XSSFCell cell = null;

        row = sheet.createRow(0);
        cell = row.createCell(0);
        XSSFCellStyle headerStyle = report.createCellStyle();
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        XSSFFont font = report.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);
        headerStyle.setFont(font); 
        headerStyle.setFillForegroundColor((short)128);
        cell.setCellStyle(headerStyle);
        cell.setCellValue("City");
        cell = row.createCell(1);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setFillForegroundColor((short)128);
        cell.setCellStyle(headerStyle);
        cell.setCellValue("Airports");
        sheet.createFreezePane(0, 1);
        int cnt = 1;
        for(String airport: airportsByCities.keySet()){
            if (cityAirportsCount.get(airport) == 1)
                continue;
            row = sheet.createRow(cnt);
            cell = row.createCell(0);
            cell.setCellValue(airport);
            cell = row.createCell(1);
            cell.setCellValue(airportsByCities.get(airport));
            cnt++;
        }
        report.write(fileOutputStream);
    }
    
    public void getCitiesWithManyCancels() throws SQLException, IOException{
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("SELECT COUNT(flight_id), city "
                    + "FROM flights "
                    + "INNER JOIN airports "
                    + "ON airports.airport_code == flights.departure_airport "
                    + "WHERE status = 'Cancelled' "
                    + "GROUP BY city "
                    + "ORDER BY COUNT(flight_id) DESC");
            
            String filename = "cities_with_many_cancels.xlsx";
            FileOutputStream fileOutputStream = new FileOutputStream(filename);
            XSSFWorkbook report = new XSSFWorkbook();
            XSSFSheet sheet = report.createSheet("cities");

            XSSFRow row = null;
            XSSFCell cell = null;

            row = sheet.createRow(0);
            cell = row.createCell(0);
            XSSFCellStyle headerStyle = report.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = report.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font); 
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("City");
            cell = row.createCell(1);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Number of cancelled flights");
            sheet.createFreezePane(0, 1);
            int cnt = 1;
            Gson gson = new Gson();
            Type type = new TypeToken<HashMap<String, String>>(){}.getType();
            while (resultSet.next()) {
                row = sheet.createRow(cnt);
                cell = row.createCell(1);
                cell.setCellValue(resultSet.getInt("COUNT(flight_id)"));
                cell = row.createCell(0);
                HashMap<String, String> city = gson.fromJson(resultSet.getString("city"), type);
                cell.setCellValue(city.get("ru"));
                cnt++;
            }
            report.write(fileOutputStream);
        }
    }
    
    public void getCityShortestPath() throws SQLException, IOException {
        @Data
        class Pair<A, B> {
            private final A first;
            private final B second;
        }
        
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("SELECT a1.city, a2.airport_name, actual_departure, actual_arrival "
                    + "FROM flights "
                    + "INNER JOIN airports a1 "
                    + "ON a1.airport_code == flights.departure_airport "
                    + "INNER JOIN airports a2 "
                    + "ON a2.airport_code == flights.arrival_airport "
                    + "WHERE actual_departure NOT NULL AND actual_arrival NOT NULL;");
            HashMap<Pair<String, String>, Pair<Long, Integer>> map = new HashMap<>();
            Gson gson = new Gson();
            Type type = new TypeToken<HashMap<String, String>>(){}.getType();
            while (resultSet.next()) {
                HashMap<String, String> city = gson.fromJson(resultSet.getString("city"), type);
                HashMap<String, String> arrival_airport = gson.fromJson(resultSet.getString("airport_name"), type);
                Pair<String, String> key = new Pair(city.get("ru"), arrival_airport.get("ru"));
                long duration = Timestamp.valueOf(resultSet.getString("actual_arrival")).getTime() - Timestamp.valueOf(resultSet.getString("actual_departure")).getTime();
                if (map.get(key) != null){
                    Pair<Long, Integer> value = map.get(key);
                    map.put(key, new Pair(value.first + duration, value.second + 1));
                } else{
                    map.put(key, new Pair(duration, 1));
                }
            }
            
            HashMap<String, Pair<String, Double>> shortest = new HashMap<>();
            for (Pair<String, String> key : map.keySet()){
                Pair<Long, Integer> value = map.get(key);
                double mean = (value.first * 1.0) / value.second;
                if (shortest.get(key.first) != null){
                    if (mean < shortest.get(key.first).second)
                        shortest.put(key.first, new Pair<>(key.second, mean));
                } else{
                    shortest.put(key.first, new Pair<>(key.second, mean));
                }
            }
            
            String filename = "cities_shortest_path.xlsx";
            FileOutputStream fileOutputStream = new FileOutputStream(filename);
            XSSFWorkbook report = new XSSFWorkbook();
            XSSFSheet sheet = report.createSheet("cities");

            XSSFRow row = null;
            XSSFCell cell = null;

            row = sheet.createRow(0);
            cell = row.createCell(0);
            XSSFCellStyle headerStyle = report.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = report.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font); 
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("City");
            cell = row.createCell(1);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Destination");
            cell = row.createCell(2);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Mean duration (hours)");
            sheet.createFreezePane(0, 1);
            int cnt = 1;
            
            ArrayList<Pair<Double, Pair<String, String>>> pairs = new ArrayList();
            
            for (String key : shortest.keySet()){
                Pair<String, Double> value = shortest.get(key);
                pairs.add(new Pair(value.second, new Pair(key, value.first)));
            }
            
            pairs.sort(Comparator.comparingDouble(Pair<Double, Pair<String, String>>::getFirst));
            
            for (Pair<Double, Pair<String, String>> pair : pairs){
                Pair<String, Double> value = shortest.get(pair.second.first);
                row = sheet.createRow(cnt);
                cell = row.createCell(0);
                cell.setCellValue(pair.second.first);
                cell = row.createCell(1);
                cell.setCellValue(value.first);
                cell = row.createCell(2);
                cell.setCellValue(value.second / (3600*1000));
                cnt++;
            }
            report.write(fileOutputStream);
        }
    }
    
    public void getCancelsByMonths() throws SQLException, IOException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
             ResultSet resultSet = stmt.executeQuery("SELECT scheduled_departure "
                     + "FROM flights "
                     + "WHERE status='Cancelled'");
            HashMap<String, Integer> map = new HashMap<>();
            while (resultSet.next()) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Timestamp.valueOf(resultSet.getString("scheduled_departure")).getTime());
                String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                if (map.get(month) != null){
                    map.put(month, map.get(month) + 1);
                } else{
                    map.put(month, 1);
                }
            }
            
            String filename = "cancels_by_months.xlsx";
            FileOutputStream fileOutputStream = new FileOutputStream(filename);
            XSSFWorkbook report = new XSSFWorkbook();
            XSSFSheet sheet = report.createSheet("Months");

            XSSFRow row = null;
            XSSFCell cell = null;

            row = sheet.createRow(0);
            cell = row.createCell(0);
            XSSFCellStyle headerStyle = report.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = report.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font); 
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Month");
            cell = row.createCell(1);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Number of cancels");
            sheet.createFreezePane(0, 1);
            int cnt = 1;
            
            final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
            
            for (String key : map.keySet()){
                int value = map.get(key);
                
                dataset.addValue(value, "cancels", key);
                
                row = sheet.createRow(cnt);
                cell = row.createCell(0);
                cell.setCellValue(key);
                cell = row.createCell(1);
                cell.setCellValue(value);
                cnt++;
            }
            report.write(fileOutputStream);

            JFreeChart barChart = ChartFactory.createBarChart(
               "Number of canceled flights", 
               "Month", "Number of cancels", 
               dataset,PlotOrientation.VERTICAL, 
               true, true, false);

            int width = 1000;    /* Width of the image */
            int height = 700;   /* Height of the image */ 
            File BarChart = new File( "cancels_by_months.jpeg" ); 
            ChartUtils.saveChartAsJPEG( BarChart , barChart , width , height );
        }
    }
    
    public void getMoscowFlights() throws SQLException, IOException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            conn.setAutoCommit(false);
            ResultSet resultSet = stmt.executeQuery("SELECT city, scheduled_departure "
                    + "FROM flights "
                    + "INNER JOIN airports "
                    + "ON departure_airport == airport_code;");
            
            HashMap<String, Integer> to = new HashMap<>();
            HashMap<String, Integer> from = new HashMap<>();
            
            DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
            String[] dayNames = symbols.getWeekdays();
            for (String s : dayNames) {
               if (s.length() == 0)
                   continue;
               to.put(s, 0);
               from.put(s, 0);
            }
            while (resultSet.next()) {
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String, String>>(){}.getType();
                HashMap<String, String> city = gson.fromJson(resultSet.getString("city"), type);
                if (!"Москва".equals(city.get("ru")))
                    continue;
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Timestamp.valueOf(resultSet.getString("scheduled_departure")).getTime());
                String day = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                from.put(day, from.get(day) + 1);
            }
            resultSet = stmt.executeQuery("SELECT city, scheduled_departure "
                    + "FROM flights "
                    + "INNER JOIN airports "
                    + "ON arrival_airport == airport_code;");
            
            while (resultSet.next()) {
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String, String>>(){}.getType();
                HashMap<String, String> city = gson.fromJson(resultSet.getString("city"), type);
                if (!"Москва".equals(city.get("ru")))
                    continue;
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Timestamp.valueOf(resultSet.getString("scheduled_departure")).getTime());
                String day = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                to.put(day, to.get(day) + 1);
            }
                
            String filename = "Moscow_flights.xlsx";
            FileOutputStream fileOutputStream = new FileOutputStream(filename);
            XSSFWorkbook report = new XSSFWorkbook();
            XSSFSheet sheet = report.createSheet("Flights");

            XSSFRow row = null;
            XSSFCell cell = null;

            row = sheet.createRow(0);
            cell = row.createCell(0);
            XSSFCellStyle headerStyle = report.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = report.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font); 
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Day");
            cell = row.createCell(1);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Flights from Moskow");
            cell = row.createCell(2);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Flights to Moskow");
            sheet.createFreezePane(0, 1);
            int cnt = 1;

            final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

            for (String key : from.keySet()){
                int from_value = from.get(key);
                int to_value = to.get(key);

                dataset.addValue(from_value, "from", key);
                dataset.addValue(to_value, "to", key);

                row = sheet.createRow(cnt);
                cell = row.createCell(0);
                cell.setCellValue(key);
                cell = row.createCell(1);
                cell.setCellValue(from_value);
                cell = row.createCell(2);
                cell.setCellValue(to_value);
                cnt++;
            }
            report.write(fileOutputStream);

            JFreeChart barChart = ChartFactory.createBarChart(
               "Number of Moscow flights", 
               "Day", "Number of flights", 
               dataset,PlotOrientation.VERTICAL, 
               true, true, false);

            int width = 1000;    /* Width of the image */
            int height = 700;   /* Height of the image */ 
            File BarChart = new File( "Moscow_flights.jpeg" ); 
            ChartUtils.saveChartAsJPEG( BarChart , barChart , width , height );
            conn.commit();
        }
    }
    
    //model should be json
    public void deleteFlights(String model) throws SQLException {
        try (Connection conn = connectionPool.getConnection()){
            conn.setAutoCommit(false); 
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM tickets "
                     + "WHERE ticket_no IN "
                     + "(SELECT ticket_no FROM ticket_flights "
                     + "WHERE flight_id IN "
                     + "(SELECT flight_id FROM flights "
                     + "WHERE aircraft_code IN "
                     + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code == ?)))")) {
                stmt.setString(1, model);
                stmt.executeUpdate();
            }
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM ticket_flights "
                     + "WHERE flight_id IN "
                     + "(SELECT flight_id FROM flights "
                     + "WHERE aircraft_code IN "
                     + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code == ?))")) {   
                stmt.setString(1, model);
                stmt.executeUpdate();
            }
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM flights "
                     + "WHERE aircraft_code IN "
                     + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code == ?)")) {   
                stmt.setString(1, model);
                stmt.executeUpdate();
            }
            conn.commit();
        }
    }
    
    public void getCancelledFlights(Timestamp from, Timestamp to) throws SQLException, IOException {
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            conn.setAutoCommit(false);
            ResultSet resultSet = stmt.executeQuery("SELECT flight_id, a1.city as from_city, a2.city as to_city, scheduled_departure "
                    + "FROM flights "
                    + "INNER JOIN airports a1 "
                    + "ON flights.departure_airport == a1.airport_code "
                    + "INNER JOIN airports a2 "
                    + "ON flights.arrival_airport == a2.airport_code "
                    + "WHERE status != \"Cancelled\"");
            
            HashMap<String, Double> loss = new HashMap<>();
            HashMap<String, StringBuilder> ids_by_day = new HashMap<>();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(from.getTime());
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            while (calendar.getTimeInMillis() < to.getTime()) {
                loss.put(df.format(calendar.getTime()), 0.0);
                ids_by_day.put(df.format(calendar.getTime()), new StringBuilder());
                calendar.add(Calendar.HOUR, 24);
            }
            
            while (resultSet.next()) {
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String, String>>(){}.getType();
                HashMap<String, String> city = gson.fromJson(resultSet.getString("from_city"), type);
                String from_city = city.get("ru");
                city = gson.fromJson(resultSet.getString("to_city"), type);
                String to_city = city.get("ru");
                if (!"Москва".equals(from_city) && !"Москва".equals(to_city))
                    continue;
                String id = resultSet.getString("flight_id");
                calendar.setTimeInMillis(Timestamp.valueOf(resultSet.getString("scheduled_departure")).getTime());
                if (calendar.getTimeInMillis() >= to.getTime())
                    continue;
                if (calendar.getTimeInMillis() < from.getTime())
                    continue;
                String day = df.format(calendar.getTime());
                StringBuilder sb = ids_by_day.get(day);
                if (sb.length() > 0)
                    sb.append(", ");
                sb.append(id);
                ids_by_day.put(day, sb);
            }
            
            for (String day : ids_by_day.keySet()){
                String value = ids_by_day.get(day).toString();
                double amount = 0;
                try (Statement Stmt = conn.createStatement()){
                    ResultSet amountRes = Stmt.executeQuery("SELECT SUM(amount) FROM ticket_flights WHERE flight_id IN ("+value+")");
                    if (amountRes.next()){
                        if (amountRes.getString("SUM(amount)") != null){
                            amount = Double.parseDouble(amountRes.getString("SUM(amount)"));
                        }
                    }
                }
                loss.put(day, amount);
                
                try (Statement Stmt = conn.createStatement()){
                    Stmt.executeUpdate("UPDATE flights SET status = \"Cancelled\" WHERE flight_id IN ("+value+")");
                }
                
            }
                
            String filename = "Covid-2017.xlsx";
            FileOutputStream fileOutputStream = new FileOutputStream(filename);
            XSSFWorkbook report = new XSSFWorkbook();
            XSSFSheet sheet = report.createSheet("Cancelled flights");

            XSSFRow row = null;
            XSSFCell cell = null;

            row = sheet.createRow(0);
            cell = row.createCell(0);
            XSSFCellStyle headerStyle = report.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = report.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font); 
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Day");
            cell = row.createCell(1);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue("Loss of money");
            sheet.createFreezePane(0, 1);
            int cnt = 1;

            final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

            for (String key : loss.keySet()){
                double amount = loss.get(key);

                dataset.addValue(amount, "amount", key);

                row = sheet.createRow(cnt);
                cell = row.createCell(0);
                cell.setCellValue(key);
                cell = row.createCell(1);
                cell.setCellValue(amount);
                cnt++;
            }
            report.write(fileOutputStream);

            JFreeChart barChart =ChartFactory.createBarChart(
               "Loss of money",
               "Day", "Amount",
               dataset,PlotOrientation.VERTICAL, 
               true, true, false);

            int width = 2500;    /* Width of the image */
            int height = 700;   /* Height of the image */ 
            File BarChart = new File( "Covid-2017.jpeg" ); 
            ChartUtils.saveChartAsJPEG( BarChart , barChart , width , height );
            conn.commit();
        }
    }
    
    public int addTickets(String ticketNo, String bookRef, String passengerId, String passengerName, HashMap<String, String> contactData, int flightId, String fareConditions, double amount, int boardingNo, String seatNo) throws SQLException{
        try (Connection conn = connectionPool.getConnection()){
            conn.setAutoCommit(false);
            String aircraftCode = "";
            try (PreparedStatement stmt = conn.prepareStatement("SELECT aircraft_code FROM flights WHERE flight_id = ?")) {
                stmt.setInt(1, flightId);
                ResultSet res = stmt.executeQuery();
                if (res.next()){
                    if (res.getString("aircraft_code") != null){
                        aircraftCode = res.getString("aircraft_code");
                    }
                    else
                        return 0;
                } else
                    return 0;
            }
            
            try (PreparedStatement stmt = conn.prepareStatement("SELECT fare_conditions FROM seats WHERE aircraft_code = ? AND seat_no = ?")) {
                stmt.setString(1, aircraftCode);
                stmt.setString(2, seatNo);
                ResultSet res = stmt.executeQuery();
                if (res.next()){
                    if (!res.getString("fare_conditions").equals(fareConditions))
                        return 0;
                } else
                    return 0;
            }
            new TicketFlightDAO(connectionPool).saveTicketFlights(Arrays.asList(new TicketFlight(ticketNo, flightId, fareConditions, amount)), conn);
            new BoardingPassDAO(connectionPool).saveBoardingPassess(Arrays.asList(new BoardingPass(ticketNo, flightId, boardingNo, seatNo)), conn);
            new TicketDAO(connectionPool).saveTickets(Arrays.asList(new Ticket(ticketNo, bookRef, passengerId, passengerName, contactData)), conn);
            conn.commit();
        }
        return 1;
    } 
}