/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines;

import org.apache.commons.dbcp2.BasicDataSource;
import javax.sql.DataSource;
/**
 *
 * @author vitalik
 */
public class SQLDriver {
    public static DataSource getSource(String connectURI){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.sqlite.JDBC");
        ds.setUrl(connectURI);
        return ds;
    }
}
