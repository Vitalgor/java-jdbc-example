/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines;

import lombok.AllArgsConstructor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.SQLException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.stream.Collectors;
import pireyvitalik.Avialines.DAO.*;
import pireyvitalik.Avialines.DTO.*;

/**
 *
 * @author vitalik
 */
@AllArgsConstructor
public class DBInit {
    private final DataSource connectionPool;
    
    private String getSQL(String name) throws IOException {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Main.class.getResourceAsStream(name),
                        StandardCharsets.UTF_8))) {
            return br.lines().collect(Collectors.joining("\n"));
        }
    }
    
    public void fillAircrafts(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<Aircraft> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new Aircraft(line));
           }
       }
       new AircraftDAO(connectionPool).saveAircrafts(data);
    }
    
    public void fillAirports(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<Airport> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new Airport(line));
           }
       }
       new AirportDAO(connectionPool).saveAirports(data);
    }

    public void fillBoardingPasses(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<BoardingPass> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new BoardingPass(line));
           }
       }
       new BoardingPassDAO(connectionPool).saveBoardingPassess(data);
    }
    
    public void fillBookings(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<Booking> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new Booking(line));
           }
       }
       new BookingDAO(connectionPool).saveBookings(data);
    }
    
    public void fillFlights(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<Flight> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new Flight(line));
           }
       }
       new FlightDAO(connectionPool).saveFlights(data);
    }
    
    public void fillSeats(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<Seat> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new Seat(line));
           }
       }
       new SeatDAO(connectionPool).saveSeats(data);
    }
    
    public void fillTickets(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<Ticket> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new Ticket(line));
           }
       }
       new TicketDAO(connectionPool).saveTickets(data);
    }
    
    public void fillTicketFlights(String csv) throws SQLException, IOException, InvalidParseException {
        Path file = Paths.get(csv);
        HashSet<TicketFlight> data = new HashSet<>();
        try (BufferedReader br =
            Files.newBufferedReader(file, StandardCharsets.UTF_8)){
           String line;
           while ((line = br.readLine())!=null){
              data.add(new TicketFlight(line));
           }
       }
       new TicketFlightDAO(connectionPool).saveTicketFlights(data);
    }
    
    public void createDB() throws SQLException, IOException {
        String sql = getSQL("DBCreate.sql");
        String [] queries = sql.split("\n\n");
        try (Connection conn = connectionPool.getConnection();
             Statement stmt = conn.createStatement()) {
            for (String query : queries){    
                stmt.executeUpdate(query);
            }
        }
    }
}
