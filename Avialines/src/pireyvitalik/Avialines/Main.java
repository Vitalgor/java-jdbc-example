/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pireyvitalik.Avialines;

import pireyvitalik.Avialines.DAO.FlightDAO;
import pireyvitalik.Avialines.DTO.Flight;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.sql.Timestamp;
import java.nio.charset.StandardCharsets;
import java.io.BufferedReader;

/**
 *
 * @author vitalik
 */
public class Main {
    public static void main(String[] args) throws SQLException, IOException, Exception {
        Path database = Paths.get("airlinesDB.sqlite");
        if ("create".equals(args[0])){
            Files.deleteIfExists(database);
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBInit dbconstructor = new DBInit(connectionPool);
            dbconstructor.createDB();
            dbconstructor.fillAircrafts("data/aircrafts_data.csv");
            dbconstructor.fillAirports("data/airports_data.csv");
            dbconstructor.fillBoardingPasses("data/boarding_passes.csv");
            dbconstructor.fillBookings("data/bookings.csv");
            dbconstructor.fillFlights("data/flights.csv");
            dbconstructor.fillSeats("data/seats.csv");
            dbconstructor.fillTickets("data/tickets.csv");
            dbconstructor.fillTicketFlights("data/ticket_flights.csv");
            return;
        }
        if ("first".equals(args[0])){
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBClient client = new DBClient(connectionPool);
            client.getCitiesWithMultipleAirports();
            return;
        }
        if ("second".equals(args[0])){
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBClient client = new DBClient(connectionPool);
            client.getCitiesWithManyCancels();
            return;
        }
        if ("third".equals(args[0])){
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBClient client = new DBClient(connectionPool);
            client.getCityShortestPath();
            return;
        }
        if ("fourth".equals(args[0])){
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBClient client = new DBClient(connectionPool);
            client.getCancelsByMonths();
            return;
        }
        if ("fifth".equals(args[0])){
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBClient client = new DBClient(connectionPool);
            client.getMoscowFlights();
            return;
        }
        if ("seventh".equals(args[0])){
            DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
            DBClient client = new DBClient(connectionPool);
            client.getCancelledFlights(Timestamp.valueOf("2017-08-01 00:00:00"), Timestamp.valueOf("2017-08-15 23:59:59"));
            return;
        }
    }
}
