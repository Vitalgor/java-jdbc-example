/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.pireyvitalik.Avialines;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.HashSet;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.HashMap;
import static org.junit.Assert.*;
import pireyvitalik.Avialines.DAO.*;
import pireyvitalik.Avialines.DBClient;
import pireyvitalik.Avialines.DBInit;
import pireyvitalik.Avialines.DTO.*;
import pireyvitalik.Avialines.SQLDriver;

/**
 *
 * @author vitalik
 */
public class DBClientTest {
    
    public DBClientTest() {
    }
    
    @Before
    public void setUp() throws IOException{
        Path database = Paths.get("airlinesDB.sqlite");
        Files.deleteIfExists(database);
        DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
        DBInit dbconstructor = new DBInit(connectionPool);
        try{
            dbconstructor.createDB();
        } catch (Exception e){
             Assert.fail("DB create failed!");
             return;
        }
        HashSet<Aircraft> data_aircraft = new HashSet<>();
        try{
            data_aircraft.add(new Aircraft("773,\"{\"\"en\"\": \"\"Boeing 777-300\"\", \"\"ru\"\": \"\"Боинг 777-300\"\"}\",11100"));
            data_aircraft.add(new Aircraft("320,\"{\"\"en\"\": \"\"Airbus A320-200\"\", \"\"ru\"\": \"\"Аэробус A320-200\"\"}\",5700"));
            new AircraftDAO(connectionPool).saveAircrafts(data_aircraft);
        } catch (Exception e){
             Assert.fail("Aircrafts add failed");
             return;
        }
        HashSet<Airport> data_airport = new HashSet<>();
        try{
            data_airport.add(new Airport("SVO,\"{\"\"en\"\": \"\"Sheremetyevo International Airport\"\", \"\"ru\"\": \"\"Шереметьево\"\"}\",\"{\"\"en\"\": \"\"Moscow\"\", \"\"ru\"\": \"\"Москва\"\"}\",\"(37.4146,55.972599)\",Europe/Moscow"));
            data_airport.add(new Airport("VKO,\"{\"\"en\"\": \"\"Vnukovo International Airport\"\", \"\"ru\"\": \"\"Внуково\"\"}\",\"{\"\"en\"\": \"\"Moscow\"\", \"\"ru\"\": \"\"Москва\"\"}\",\"(37.2615013123,55.5914993286)\",Europe/Moscow"));
            data_airport.add(new Airport("TOF,\"{\"\"en\"\": \"\"Bogashevo Airport\"\", \"\"ru\"\": \"\"Богашёво\"\"}\",\"{\"\"en\"\": \"\"Tomsk\"\", \"\"ru\"\": \"\"Томск\"\"}\",\"(85.208297729492,56.380298614502)\",Asia/Krasnoyarsk"));
            
            new AirportDAO(connectionPool).saveAirports(data_airport);
        } catch (Exception e){
             Assert.fail("Airports add failed");
             return;
        }
        HashSet<Flight> data_flight = new HashSet<>();
        try{
            data_flight.add(new Flight("1185,PG0134,2017-08-08 09:50:00+03,2017-09-10 14:55:00+03,TOF,TOF,Scheduled,773,,"));
            data_flight.add(new Flight("1186,PG0134,2017-08-08 09:50:00+03,2017-09-10 14:55:00+03,TOF,TOF,Scheduled,320,,"));
            data_flight.add(new Flight("1187,PG0134,2017-08-08 09:50:00+03,2017-09-10 14:55:00+03,SVO,TOF,Scheduled,320,,"));
            data_flight.add(new Flight("1188,PG0134,2017-08-08 09:50:00+03,2017-09-10 14:55:00+03,TOF,VKO,Scheduled,320,,"));
            data_flight.add(new Flight("1189,PG0134,2018-08-08 09:50:00+03,2017-09-10 14:55:00+03,TOF,VKO,Scheduled,320,,"));
            new FlightDAO(connectionPool).saveFlights(data_flight);
        } catch (Exception e){
             Assert.fail("Flights add failed");
             return;
        }
        HashSet<Seat> data_seat = new HashSet<>();
        try{
            data_seat.add(new Seat("773,2A,Business"));
            data_seat.add(new Seat("320,2A,Economy"));
            new SeatDAO(connectionPool).saveSeats(data_seat);
        } catch (Exception e){
             Assert.fail("Seats add failed");
             return;
        }
        HashSet<TicketFlight> data_tf = new HashSet<>();
        try{
            data_tf.add(new TicketFlight("0005432159776,1185,Business,100.00"));
            data_tf.add(new TicketFlight("0005432159777,1186,Economy,100.00"));
            data_tf.add(new TicketFlight("0005432159778,1187,Economy,100.00"));
            new TicketFlightDAO(connectionPool).saveTicketFlights(data_tf);
        } catch (Exception e){
             Assert.fail("Seats add failed");
             return;
        }
        HashSet<Ticket> data_ticket = new HashSet<>();
        try{
            data_ticket.add(new Ticket("0005432159776,06B046,8149 604011,VALERIY TIKHONOV,\"{\"\"phone\"\": \"\"+70127117011\"\"}\""));
            data_ticket.add(new Ticket("0005432159777,06B046,8149 604011,VALERIY TIKHONOV,\"{\"\"phone\"\": \"\"+70127117011\"\"}\""));
            data_ticket.add(new Ticket("0005432159778,06B046,8149 604011,VALERIY TIKHONOV,\"{\"\"phone\"\": \"\"+70127117011\"\"}\""));
            new TicketDAO(connectionPool).saveTickets(data_ticket);
        } catch (Exception e){
             Assert.fail("Tickets add failed");
             return;
        }
        
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testDeleteFlights() throws Exception {
        DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
        DBClient client = new DBClient(connectionPool);
        String model = "{\"en\": \"Boeing 777-300\", \"ru\": \"Боинг 777-300\"}";
        client.deleteFlights(model);
        try (Connection conn = connectionPool.getConnection()){
            try (PreparedStatement stmt = conn.prepareStatement("SELECT ticket_no FROM tickets "
                         + "WHERE ticket_no IN "
                         + "(SELECT ticket_no FROM ticket_flights "
                         + "WHERE flight_id IN "
                         + "(SELECT flight_id FROM flights "
                         + "WHERE aircraft_code IN "
                         + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code == ?)))")) {
                    stmt.setString(1, model);
                    ResultSet resultSet = stmt.executeQuery();
                    if (resultSet.next()) {
                        Assert.fail("Flights delete failed");
                        return;
                    }            
            } catch (Exception e){
                 Assert.fail("Flights delete failed");
                 return;
            }
            try (PreparedStatement stmt = conn.prepareStatement("SELECT ticket_no FROM tickets "
                         + "WHERE ticket_no IN "
                         + "(SELECT ticket_no FROM ticket_flights "
                         + "WHERE flight_id IN "
                         + "(SELECT flight_id FROM flights "
                         + "WHERE aircraft_code IN "
                         + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code != ?)))")) {
                    stmt.setString(1, model);
                    ResultSet resultSet = stmt.executeQuery();
                    if (!resultSet.next()) {
                        Assert.fail("Flights delete failed");
                        return;
                    }            
            } catch (Exception e){
                 Assert.fail("Flights delete failed");
                 return;
            }
            try (PreparedStatement stmt = conn.prepareStatement("SELECT ticket_no FROM ticket_flights "
                         + "WHERE flight_id IN "
                         + "(SELECT flight_id FROM flights "
                         + "WHERE aircraft_code IN "
                         + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code == ?))")) {   
                    stmt.setString(1, model);
                    ResultSet resultSet = stmt.executeQuery();
                    if (resultSet.next()) {
                        Assert.fail("Flights delete failed");
                        return;
                    }
            } catch (Exception e){
                 Assert.fail("Flights delete failed");
                 return;
            }
            try (PreparedStatement stmt = conn.prepareStatement("SELECT flight_id FROM flights "
                         + "WHERE aircraft_code IN "
                         + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code == ?)")) {   
                    stmt.setString(1, model);
                    ResultSet resultSet = stmt.executeQuery();
                    if (resultSet.next()) {
                        Assert.fail("Flights delete failed");
                        return;
                    }
            } catch (Exception e){
                 Assert.fail("Flights delete failed");
                 return;
            }
            try (PreparedStatement stmt = conn.prepareStatement("SELECT flight_id FROM flights "
                         + "WHERE aircraft_code IN "
                         + "(SELECT aircraft_code FROM aircrafts WHERE aircraft_code != ?)")) {   
                    stmt.setString(1, model);
                    ResultSet resultSet = stmt.executeQuery();
                    if (!resultSet.next()) {
                        Assert.fail("Flights delete failed");
                        return;
                    }
            } catch (Exception e){
                 Assert.fail("Flights delete failed");
                 return;
            }
        } catch (Exception e){
            Assert.fail("Flights delete failed");
            return;
       }
    }

    @Test
    public void testGetCancelledFlights() throws Exception {
        DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
        DBClient client = new DBClient(connectionPool);
        client.getCancelledFlights(Timestamp.valueOf("2017-08-01 00:00:00"), Timestamp.valueOf("2017-08-15 23:59:59"));
        try (Connection conn = connectionPool.getConnection()){
          try (Statement stmt = conn.createStatement()) {   
                   ResultSet resultSet = stmt.executeQuery("SELECT status FROM flights "
                        + "WHERE flight_id IN (1187, 1188)");
                   while (resultSet.next()) {
                       Assert.assertEquals("Cancelled", resultSet.getString("status"));
                   }
           } catch (Exception e){
                Assert.fail("Get covid-2017 cancelled flights failed");
                return;
           }
          try (Statement stmt = conn.createStatement()) {   
                   ResultSet resultSet = stmt.executeQuery("SELECT status FROM flights "
                        + "WHERE flight_id IN (1185, 1186, 1189)");
                   while (resultSet.next()) {
                       Assert.assertEquals("Scheduled", resultSet.getString("status"));
                   }
           } catch (Exception e){
                Assert.fail("Get covid-2017 cancelled flights failed");
                return;
           }
        } catch (Exception e){
            Assert.fail("Get covid-2017 cancelled flights failed");
            return;
       }
    }

    @Test
    public void testAddTickets() throws Exception {
        DataSource connectionPool = SQLDriver.getSource("jdbc:sqlite:airlinesDB.sqlite");
        DBClient client = new DBClient(connectionPool);
        Assert.assertEquals(0, client.addTickets("0000000000001","06B046", "8149", "Oleg Ivcenko", null, 1185, "Business", 100, 1, "2B"));
        Assert.assertEquals(0, client.addTickets("0000000000001","06B046", "8149", "Oleg Ivcenko", null, 1185, "Economy", 100, 1, "2A"));
        Assert.assertEquals(0, client.addTickets("0000000000001","06B046", "8149", "Oleg Ivcenko", null, 1000, "Business", 100, 1, "2A"));
        Assert.assertEquals(1, client.addTickets("0000000000001","06B046", "8149", "Oleg Ivcenko", null, 1185, "Business", 100, 1, "2A"));
        try (Connection conn = connectionPool.getConnection()){
          try (Statement stmt = conn.createStatement()) {   
                   ResultSet resultSet = stmt.executeQuery("SELECT * FROM tickets "
                        + "WHERE ticket_no == \"0000000000001\"");
                   while (resultSet.next()) {
                       Assert.assertEquals("06B046", resultSet.getString("book_ref"));
                       Assert.assertEquals("8149", resultSet.getString("passenger_id"));
                       Assert.assertEquals("Oleg Ivcenko", resultSet.getString("passenger_name"));
                   }
           } catch (Exception e){
                Assert.fail("Ticket add failed failed");
                return;
           }
          try (Statement stmt = conn.createStatement()) {   
                   ResultSet resultSet = stmt.executeQuery("SELECT * FROM ticket_flights "
                        + "WHERE ticket_no == \"0000000000001\"");
                   while (resultSet.next()) {
                       Assert.assertEquals(1185, resultSet.getInt("flight_id"));
                       Assert.assertEquals("Business", resultSet.getString("fare_conditions"));
                       Assert.assertEquals(100.00, resultSet.getDouble("amount"), 0.0001d);
                   }
           } catch (Exception e){
                Assert.fail("Ticket add failed failed");
                return;
           }
          try (Statement stmt = conn.createStatement()) {   
                   ResultSet resultSet = stmt.executeQuery("SELECT * FROM boarding_passes "
                        + "WHERE ticket_no == \"0000000000001\"");
                   while (resultSet.next()) {
                       Assert.assertEquals(1185, resultSet.getInt("flight_id"));
                       Assert.assertEquals(1, resultSet.getInt("boarding_no"));
                       Assert.assertEquals("2A", resultSet.getString("seat_no"));
                   }
           } catch (Exception e){
                Assert.fail("Ticket add failed failed");
                return;
           }
        }
    }
    
}
